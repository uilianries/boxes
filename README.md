# libboxes

[![pipeline status](https://gitlab.com/twdev_projects/boxes/badges/master/pipeline.svg)](https://gitlab.com/twdev_projects/boxes/-/commits/master)

Boxes is a set of frequently used containers built on top of STL.

Data structures within this library are implemented without any use-case
specific optimisations applied on top.  The purpose is to have a more rich
catalogue of ready-made containers for general purposes.

## Building

The library supports both meson and CMake build systems.

### meson

    meson setup bld
    meson configure -Dbuildtype=release bld
    meson compile -C bld
    meson install -C bld

### CMake

Conan is required to install dependencies.

    conan install -of bld --build=missing --settings=build_type=Release .
    source bld/build/Release/generators/conanbuild.sh

#### Building with cmake presets

    cmake --preset conan-release
    cmake --build --preset conan-release

#### Building traditional way

    cmake \
        -Bbld/build/Release/generators \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake
        -G Ninja

    ninja -C bld/build/Release/generators install

## Using libboxes in Conan/CMake project

In your conanfile.txt add:

```
[requires]
libboxes/0.1
```

In your `CMakeLists.txt` add:

```
...
find_package(libboxes REQUIRED)

...

target_link_libraries(myexe libboxes::libboxes)
```

For local development you don't have to rely on conan center.  Just run:

    conan create .

`libboxes` is built and cached in your local conan cache now.


## Documentation

Doxygen documentation is available [here](https://boxes-twdev-projects-ba06c75b02a9b24f40d3a6b9b7130a3266c6a3c532.gitlab.io/).

### Containers catalogue

- RingBuffer
- LinkedVector
- Cache with LRU/MRU/LFU eviction policies
- Bloom filter with a customisable set of hash functions
    - Murmur2A
    - Bob Jenkins' One at a time
    - XXHash64

## TODO

- [x] Implement [Bloom filter](https://en.wikipedia.org/wiki/Bloom_filter)
- [ ] Implement [Cuckoo filter](https://en.wikipedia.org/wiki/Cuckoo_filter)
- [ ] Implement [Skip list](https://en.wikipedia.org/wiki/Skip_list)
- [x] Add LICENSE file
- [x] Add examples
- [x] Add Doxygen documentation for LinkedVector
- [x] Add Doxygen documentation for Cache
- [x] Add bloom filter examples (generic one + different hash families usage)
