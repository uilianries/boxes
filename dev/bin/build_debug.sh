#!/usr/bin/env bash

BUILD_DIR="$PWD/build"

[ -d "$BUILD_DIR" ] || meson setup build

meson configure -Dbuildtype=debug build

meson compile -C build --clean

meson compile -C build
