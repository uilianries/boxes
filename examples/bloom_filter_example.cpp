#include <boxes/bloom_filter.hpp>

#include <iostream>

int main(int argc, const char *argv[]) {
  using namespace boxes::hash;
  auto filter = boxes::filter::bloom::makeWithHashWithSeedFamily<int, XXHash64>(
      1000, 0.001);

  filter.insert(1);
  filter.insert(2);
  filter.insert(3);

  std::cout << "Contains 1: " << filter.contains(1) << std::endl;
  std::cout << "Contains 2: " << filter.contains(2) << std::endl;
  std::cout << "Contains 3: " << filter.contains(3) << std::endl;

  std::cout << "Contains 4: " << filter.contains(4) << std::endl;
  std::cout << "Contains 5: " << filter.contains(5) << std::endl;

  return 0;
}
