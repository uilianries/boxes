#include <boxes/linked_vector.hpp>

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

int main(int, const char *[]) {
  std::mutex m;
  std::condition_variable cv;

  boxes::LinkedVector<int, 4096> vec;

  std::thread producer([&] {
    for (int i = 0; i < 32; ++i) {
      std::unique_lock<std::mutex> lk(m);
      vec.push_back(i);
      cv.notify_one();
    }
  });

  std::thread consumer([&] {
    for (int i = 0; i < 32; ++i) {
      std::unique_lock<std::mutex> lk(m);
      cv.wait(lk, [&vec] { return !vec.empty(); });
      std::cout << vec.front() << std::endl;
      vec.pop_front();
    }
  });

  producer.join();
  consumer.join();

  return 0;
}
