#include <boxes/ring_buffer.hpp>

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

int main(int, const char *[]) {
  std::mutex m;
  std::condition_variable cv;

  // fixed-size ring buffer
  boxes::RingBuffer<int, 8> buf;

  std::thread producer([&] {
    for (int i = 0; i < 32; ++i) {
      std::unique_lock<std::mutex> lk(m);
      cv.wait(lk, [&buf] { return !buf.full(); });

      if (!buf.push_back(i)) {
        std::cerr << "Failed to push_back" << std::endl;
      }

      cv.notify_one();
    }
  });

  std::thread consumer([&] {
    for (int i = 0; i < 32; ++i) {
      std::unique_lock<std::mutex> lk(m);
      cv.wait(lk, [&] { return !buf.empty(); });

      std::cout << buf.front() << std::endl;
      if (!buf.pop_front()) {
        std::cerr << "Failed to pop_front" << std::endl;
      }

      cv.notify_one();
    }
  });

  producer.join();
  consumer.join();

  return 0;
}
