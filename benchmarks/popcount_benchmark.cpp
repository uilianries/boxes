#include <benchmark/benchmark.h>

#include <algorithm>
#include <bit>
#include <execution>
#include <vector>

static void BM_stdvector_count(benchmark::State &state) {
  std::vector<bool> v(1 << 10, 1);
  for (auto _ : state) {
    benchmark::DoNotOptimize(std::count(v.begin(), v.end(), 1));
  }
}

static void BM_stdvector_popcount(benchmark::State &state) {
  std::vector<uint64_t> v(1 << 10, 1);
  for (auto _ : state) {
    auto c = 0;
    for (const auto &x : v) {
      c += std::popcount(x);
    }
    benchmark::DoNotOptimize(c);
    benchmark::ClobberMemory();
  }
}

static void BM_stdvector_popcount2(benchmark::State &state) {
  std::vector<uint64_t> v(1 << 10, 1);
  for (auto _ : state) {
    benchmark::DoNotOptimize(std::accumulate(
        v.begin(), v.end(), 0,
        [](const auto &a, const auto &b) { return a + std::popcount(b); }));
    benchmark::ClobberMemory();
  }
}

static void BM_stdvector_popcount_par(benchmark::State &state) {
  std::vector<uint64_t> v(1 << 10, 1);
  for (auto _ : state) {
    std::vector<std::size_t> counts(v.size());
    std::transform(std::execution::par, v.begin(), v.end(), counts.begin(),
                   [](const auto &base) { return std::popcount(base); });
    auto c = std::accumulate(counts.begin(), counts.end(), 0);
    benchmark::DoNotOptimize(c);
    benchmark::ClobberMemory();
  }
}

BENCHMARK(BM_stdvector_count);
BENCHMARK(BM_stdvector_popcount);
BENCHMARK(BM_stdvector_popcount2);
BENCHMARK(BM_stdvector_popcount_par);

BENCHMARK_MAIN();
