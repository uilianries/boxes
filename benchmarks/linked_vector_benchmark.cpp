#include <benchmark/benchmark.h>

#include <deque>

#include <boxes/linked_vector.hpp>

using namespace boxes;

static void BM_LinkedVector_PushFront(benchmark::State &state) {
  LinkedVector<int, 4096> linked_vector{};

  for (auto _ : state) {
    linked_vector.push_front(state.iterations());
  }
}

static void BM_LinkedVector_PushBack(benchmark::State &state) {
  LinkedVector<int, 4096> linked_vector{};

  for (auto _ : state) {
    linked_vector.push_back(state.iterations());
  }
}

static void BM_LinkedVector_PopFront(benchmark::State &state) {
  LinkedVector<int, 4096> linked_vector{};

  for (auto _ : state) {
    linked_vector.push_front(state.iterations());
    linked_vector.pop_front();
  }
}

static void BM_LinkedVector_PopBack(benchmark::State &state) {
  LinkedVector<int, 4096> linked_vector{};

  for (auto _ : state) {
    linked_vector.push_back(state.iterations());
    linked_vector.pop_back();
  }
}

static void BM_LinkedVector_PushPopChunksFront(benchmark::State &state) {
  constexpr std::size_t cs = 4096;
  LinkedVector<int, cs> linked_vector{};
  const auto m = 3;

  for (auto _ : state) {
    for (std::size_t r = 0; r < 10; r++) {
      for (std::size_t i = 0; i < cs * m; ++i) {
        linked_vector.push_front(state.iterations());
      }

      for (std::size_t i = 0; i < cs * m; ++i) {
        linked_vector.pop_front();
      }
    }
  }
}

static void BM_Deque_PushPopChunksFront(benchmark::State &state) {
  std::deque<int> deq{};
  const auto cs = 4096;
  const auto m = 3;

  for (auto _ : state) {
    for (std::size_t r = 0; r < 10; r++) {
      for (std::size_t i = 0; i < cs * m; ++i) {
        deq.push_front(state.iterations());
      }

      for (std::size_t i = 0; i < cs * m; ++i) {
        deq.pop_front();
      }
    }
  }
}

static void BM_Deque_PushFront(benchmark::State &state) {
  std::deque<int> deq{};

  for (auto _ : state) {
    deq.push_front(state.iterations());
  }
}

static void BM_Deque_PushBack(benchmark::State &state) {
  std::deque<int> deq{};

  for (auto _ : state) {
    deq.push_back(state.iterations());
  }
}

static void BM_Deque_PopFront(benchmark::State &state) {
  std::deque<int> deq{};

  for (auto _ : state) {
    deq.push_front(state.iterations());
    deq.pop_front();
  }
}

static void BM_Deque_PopBack(benchmark::State &state) {
  std::deque<int> deq{};

  for (auto _ : state) {
    deq.push_back(state.iterations());
    deq.pop_back();
  }
}

BENCHMARK(BM_LinkedVector_PushFront);
BENCHMARK(BM_LinkedVector_PushBack);
BENCHMARK(BM_LinkedVector_PopFront);
BENCHMARK(BM_LinkedVector_PopBack);
BENCHMARK(BM_LinkedVector_PushPopChunksFront);
BENCHMARK(BM_Deque_PushFront);
BENCHMARK(BM_Deque_PushBack);
BENCHMARK(BM_Deque_PopFront);
BENCHMARK(BM_Deque_PopBack);
BENCHMARK(BM_Deque_PushPopChunksFront);

// Run the benchmark
BENCHMARK_MAIN();
