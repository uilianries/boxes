/**
 * Copyright 2024 Tomasz Wisniewski <twdev.blogger@gmail.com>
 *
 * @file linked_vector.hpp
 *
 * @brief Implements a vector-like container that uses a linked list of
 * fixed-size chunks to store its elements.
 *
 * @example linked_vector_example.cpp
 *
 */
#ifndef __BOXES_LINKED_VECTOR_HPP__
#define __BOXES_LINKED_VECTOR_HPP__

#include "compiler.hpp"
#include "ring_buffer.hpp"

#include <list>
#include <memory>
#include <stdexcept>

namespace boxes {

/**
 * @brief Implements a vector-like container that uses a linked list of
 * fixed-size chunks to store its elements.
 *
 * The LinkedVector class is a container that provides a similar interface to
 * std::vector, but uses a linked list of fixed-size chunks to store its
 * elements. This allows for efficient insertion and deletion at both ends of
 * the container, as well as efficient memory usage for large containers.
 *
 * Internally, the LinkedVector uses RingBuffer objects as chunks to store its
 * elements.
 *
 * To minimise the number of memory allocations, the LinkedVector maintains a
 * list of discarded chunks that can be reused when new chunks are needed. When
 * a chunk is no longer needed, it is moved to the list of discarded chunks
 * rather than being deallocated.
 *
 * @tparam T The type of the elements stored in the container.
 * @tparam ChunkSizeV The size of the chunks used to store the elements.
 */
template <typename T, std::size_t ChunkSizeV> class LinkedVector {
public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;

  static constexpr std::size_t ChunkSize = ChunkSizeV;

  class iterator {
  public:
    using value_type = T;
    using reference = T &;
    using pointer = T *;
    using difference_type = std::ptrdiff_t;
    using iterator_category = std::random_access_iterator_tag;

    iterator(LinkedVector<T, ChunkSizeV> *cnt, std::size_t pos)
        : cnt{cnt}, pos{pos} {}

    iterator &operator++() {
      if (pos != cnt->size()) {
        ++pos;
      }
      return *this;
    }

    iterator operator++(int) {
      iterator tmp{*this};
      ++(*this);
      return tmp;
    }

    iterator &operator--() {
      if (pos != 0) {
        --pos;
      }
      return *this;
    }

    iterator operator--(int) {
      iterator tmp{*this};
      --(*this);
      return tmp;
    }

    reference operator*() { return cnt->at(pos); }

    pointer operator->() { return &cnt->at(pos); }

    bool operator==(const iterator &other) const {
      return cnt == other.cnt && pos == other.pos;
    }

    bool operator!=(const iterator &other) const { return !(*this == other); }

  protected:
    LinkedVector<T, ChunkSizeV> *cnt;
    std::size_t pos;
  };

  /**
   * @brief Constructs an empty container
   *
   * @note By default, the LinkedVector is constructed with a single empty
   * chunk.
   */
  LinkedVector() { initialise(); }

  LinkedVector(const LinkedVector<T, ChunkSizeV> &other) = delete;

  /**
   * @brief Move constructor
   *
   * @param other The LinkedVector to move from
   */
  LinkedVector(LinkedVector<T, ChunkSizeV> &&other)
      : chunks{std::move(other.chunks)},
        discardedChunks{std::move(other.discardedChunks)},
        frontChunk{other.frontChunk}, backChunk{other.backChunk} {
    other.frontChunk = nullptr;
    other.backChunk = nullptr;
  }

  LinkedVector<T, ChunkSizeV> &
  operator=(const LinkedVector<T, ChunkSizeV> &other) = delete;

  /**
   * @brief Move assignment operator
   *
   * @param other The LinkedVector to move from
   * @return A reference to this LinkedVector
   */
  LinkedVector<T, ChunkSizeV> &operator=(LinkedVector<T, ChunkSizeV> &&other) {
    if (this != &other) {
      LinkedVector<T, ChunkSizeV> tmp(std::move(other));
      swap(tmp);
    }
    return *this;
  }

  /**
   * @brief Swaps the contents of this LinkedVector with another
   *
   * @note The pool of discarded chunks is swapped as well.
   *
   * @param other The LinkedVector to swap with
   */
  void swap(LinkedVector<T, ChunkSizeV> &other) {
    std::swap(chunks, other.chunks);
    std::swap(discardedChunks, other.discardedChunks);
    std::swap(frontChunk, other.frontChunk);
    std::swap(backChunk, other.backChunk);
  }

  /**
   * @brief Pushes a new element to the front of the container
   *
   * Newly inserted element is retrievable with `front` method.
   *
   * The time complexity of this operation is O(1).
   *
   * May incur a chunk allocation if the front chunk is full and the pool of
   * discarded chunks is empty.
   *
   * @tparam Arg The type of the argument to push
   * @param value The value to push to the front of the container
   */
  template <typename Arg> void push_front(Arg &&value) {
    expandFront();
    frontChunk->push_front(std::forward<Arg>(value));
  }

  /**
   * @brief Pushes a new element to the back of the container
   *
   * Newly inserted element is retrievable with `back` method.
   *
   * The time complexity of this operation is O(1).
   *
   * May incur a chunk allocation if the back chunk is full and the pool of
   * discarded chunks is empty.
   *
   * @tparam Arg The type of the argument to push
   * @param value The value to push to the back of the container
   */
  template <typename Arg> void push_back(Arg &&value) {
    expandBack();
    backChunk->push_back(std::forward<Arg>(value));
  }

  /**
   * @brief Removes the first element from the container
   *
   * The time complexity of this operation is O(1).
   *
   * If the front chunk becomes empty after the removal, it is moved to the pool
   * of discarded chunks.  No deallocation is performed.
   */
  void pop_front() {
    if (frontChunk == backChunk && frontChunk->empty()) {
      return;
    }

    frontChunk->pop_front();
    trim_front();
  }

  /**
   * @brief Removes the last element from the container
   *
   * The time complexity of this operation is O(1).
   *
   * If the back chunk becomes empty after the removal, it is moved to the pool
   * of discarded chunks.  No deallocation is performed.
   */
  void pop_back() {
    if (frontChunk == backChunk && backChunk->empty()) {
      return;
    }

    backChunk->pop_back();
    trim_back();
  }

  /**
   * @brief Returns a reference to the first element in the container
   *
   * If the container is empty, an exception of type std::out_of_range is
   * thrown.
   *
   * @return Reference to the first element in the container
   */
  T &front() {
    if (frontChunk->empty()) {
      throw std::out_of_range("LinkedVector::front");
    }

    return frontChunk->front();
  }

  /**
   * @brief Returns a const reference to the first element in the container
   *
   * If the container is empty, an exception of type std::out_of_range is
   * thrown.
   *
   * @return const T & A const reference to the first element in the container
   */
  const T &front() const { return const_cast<LinkedVector *>(this)->front(); }

  /**
   * @brief Returns a reference to the last element in the container
   *
   * If the container is empty, an exception of type std::out_of_range is
   * thrown.
   *
   * @return Reference to the last element in the container
   */
  T &back() {
    if (backChunk->empty()) {
      throw std::out_of_range("LinkedVector::back");
    }

    return backChunk->back();
  }

  /**
   * @brief Returns a const reference to the last element in the container
   *
   * @return const T & A const reference to the last element in the container
   */
  const T &back() const { return const_cast<LinkedVector *>(this)->back(); }

  /**
   * @brief Provides access to the element at the specified position
   *
   * No bounds checking is performed.
   *
   * @param pos The position of the element to access
   * @return A reference to the element at the specified position
   */
  T &operator[](size_type pos) {
    if (const auto &fs = frontChunk->size(); pos < fs) {
      return frontChunk->operator[](pos);
    } else {
      pos -= fs;
      auto chunkIndex = pos / ChunkSizeV + 1;
      auto chunkOffset = pos % ChunkSizeV;

      auto it = chunks.begin();
      std::advance(it, chunkIndex);
      return it->get()->operator[](chunkOffset);
    }

    boxes_unreachable();
  }

  /**
   * @brief Returns a const reference to the element at the specified position
   *
   * No bounds checking is performed.
   *
   * @param pos The position of the element to access
   * @return  A const reference to the element at the specified position
   */
  const T &operator[](size_type pos) const {
    return const_cast<LinkedVector *>(this)->operator[](pos);
  }

  /**
   * @brief Returns a reference to the element at the specified position
   *
   * If pos is not within the range of the container, an exception of type
   * std::out_of_range is thrown.
   *
   * @param pos The position of the element to access
   * @return A reference to the element at the specified position
   */
  T &at(size_type pos) {
    if (pos >= size()) {
      throw std::out_of_range("LinkedVector::at");
    }
    return (*this)[pos];
  }

  /**
   * @brief Returns a const reference to the element at the specified position
   *
   * If pos is not within the range of the container, an exception of type
   * std::out_of_range is thrown.
   *
   * @param pos The position of the element to access
   * @return A const reference to the element at the specified position
   */
  const T &at(size_type pos) const {
    return const_cast<LinkedVector *>(this)->at(pos);
  }

  /**
   * @brief Returns an iterator to the first element of the container
   *
   * @return an iterator to the first element of the container
   */
  iterator begin() { return iterator(this, 0); }

  /**
   * @brief Returns an iterator to the element following the last element of the
   * container
   *
   * @return an iterator to the element following the last element of the
   * container
   */
  iterator end() { return iterator(this, size()); }

  /**
   * @brief Returns a const iterator to the first element of the container
   *
   * @return an iterator to the first element of the container
   */
  iterator cbegin() const { return begin(); }

  /**
   * @brief Returns a const iterator to the element following the last element
   *
   * @return an iterator to the element following the last element of the
   * container
   */
  iterator cend() const { return end(); }

  /**
   * @brief Returns true if the two containers are equal
   *
   * The two containers are considered equal if they have the same number of
   * elements and all the elements, compared in order, are equal.
   *
   * @param other The container to compare with
   * @return true if the two containers are equal, false otherwise
   */
  bool operator==(const LinkedVector<T, ChunkSizeV> &other) const {
    if (size() != other.size()) {
      return false;
    }

    for (auto it = chunks.cbegin(), otherIt = other.chunks.cbegin();
         it != chunks.cend(); ++it, ++otherIt) {
      if (**it != **otherIt) {
        return false;
      }
    }

    return true;
  }

  /**
   * @brief Returns true if the two containers are not equal
   *
   * @param other  The container to compare with
   * @return  true if the two containers are not equal, false otherwise
   */
  bool operator!=(const LinkedVector<T, ChunkSizeV> &other) const {
    return !(*this == other);
  }

  /**
   * @brief Returns the number of elements in the container
   *
   * @return The number of elements in the container
   */
  std::size_t size() const noexcept {
    switch (chunks.size()) {
    case 0:
      return 0;
    case 1:
      return frontChunk->size();
    case 2:
      return frontChunk->size() + backChunk->size();
    default:
      return frontChunk->size() + backChunk->size() +
             (chunks.size() - 2) * ChunkSizeV;
    }

    boxes_unreachable();
  }

  /**
   * @brief Returns the maximum number of elements the container can hold
   * without additional memory allocation
   *
   * The capacity of a LinkedVector is the maximum number of elements it can
   * hold without additional memory allocation. This is the number of elements
   * that can be stored in the currently allocated chunks.
   *
   * @note The capacity of a LinkedVector is always a multiple of the chunk
   * size.
   *
   * @return The maximum number of elements the container can hold without any
   * additional memory allocation.
   */
  std::size_t capacity() const noexcept { return chunks.size() * ChunkSizeV; }

  /**
   * @brief Removes all elements from the container
   */
  void clear() {
    chunks.clear();
    discardedChunks.clear();
    initialise();
  }

  /**
   * @brief Returns true if the container is empty.
   *
   * @return true if the container is empty, false otherwise.
   */
  bool empty() const noexcept { return size() == 0; }

protected:
  using Chunk = RingBuffer<T, ChunkSizeV>;
  std::list<std::unique_ptr<Chunk>> chunks;
  std::list<std::unique_ptr<Chunk>> discardedChunks;
  Chunk *frontChunk{nullptr};
  Chunk *backChunk{nullptr};

  void trim_front() {
    if (frontChunk != backChunk && frontChunk->empty()) {
      discardedChunks.push_back(std::move(chunks.front()));
      chunks.pop_front();
      frontChunk = chunks.front().get();
    }
  }

  void trim_back() {
    if (frontChunk != backChunk && backChunk->empty()) {
      discardedChunks.push_back(std::move(chunks.back()));
      chunks.pop_back();
      backChunk = chunks.back().get();
    }
  }

  void expandFront() {
    if (frontChunk->full()) {
      auto c = getChunk();
      frontChunk = c.get();
      chunks.push_front(std::move(c));
    }
  }

  void expandBack() {
    if (backChunk->full()) {
      auto c = getChunk();
      backChunk = c.get();
      chunks.push_back(std::move(c));
    }
  }

  std::unique_ptr<Chunk> getChunk() {
    if (discardedChunks.empty()) {
      return std::make_unique<Chunk>();
    }

    auto c = std::move(discardedChunks.front());
    c->clear();
    discardedChunks.pop_front();
    return c;
  }

  void initialise() {
    chunks.push_back(getChunk());
    frontChunk = chunks.front().get();
    backChunk = chunks.back().get();
  }
};

} // namespace boxes

#endif // __BOXES_LINKED_VECTOR_HPP__
