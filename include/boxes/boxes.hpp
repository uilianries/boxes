/**
 * @mainpage
 *
 * @section intro Introduction
 *
 * This is a collection of C++ containers that I find useful in my day-to-day
 * work.  Data structures are implemented in a way that is easy to understand
 * and use.  There are no optimizations for performance or for any specific use
 * case.
 *
 * @section containers Containers
 *
 * Please, refer to examples for basic usage and more information.
 *
 * - @ref boxes::cache::Cache
 *   - An implementation of a Cache with LRU/MRU and LFU eviction policies.
 *   - Example: @ref simple_lru_example.cpp
 *   - Header: @ref cache.hpp
 *
 * - @ref boxes::LinkedVector
 *   - A vector that is implemented as a linked list of fixed-size chunks.  This
 * allows for efficient insertion and deletion at the front and back of the
 * vector.
 *   - Example: @ref linked_vector_example.cpp
 *   - Header: @ref linked_vector.hpp
 *
 * - @ref boxes::RingBuffer
 *   - A fixed-size ring buffer allowing for efficient insertion and deletion
 * at both ends.
 *   - Example: @ref ring_buffer_example.cpp
 *   - Header: @ref ring_buffer.hpp
 *
 * - @ref boxes::filter::bloom::Bloom
 *   - A generic bloom filter implementation.
 *   - Example: @ref bloom_filter_example.cpp
 *   - Header: @ref bloom_filter.hpp
 *
 */
#ifndef __BOXES_BOXES_HPP__
#define __BOXES_BOXES_HPP__

#include "bloom_filter.hpp"
#include "cache.hpp"
#include "hashes.hpp"
#include "linked_vector.hpp"
#include "ring_buffer.hpp"

#endif // __BOXES_BOXES_HPP__
