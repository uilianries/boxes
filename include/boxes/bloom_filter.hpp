/**
 * Copyright 2024 Tomasz Wisniewski <twdev.blogger@gmail.com>
 *
 * @file bloom_filter.hpp
 *
 * @brief Implements a generic bloom filter.  The bloom filter allows for
 * customisation of used hash functions and hash functions.
 *
 * @example bloom_filter_example.cpp
 */

#ifndef __BOXES_BLOOM_FILTER_HPP__
#define __BOXES_BLOOM_FILTER_HPP__

#include <boxes/hash_utils.hpp>
#include <boxes/hashes.hpp>

#include <algorithm>
#include <bit>
#include <cmath>
#include <numeric>
#include <vector>

namespace boxes::filter::bloom {

/**
 * @brief A generic bloom filter implementation
 *
 * @tparam KeyT type of keys the filter will store
 * @tparam HashT type of the hash family to use see `HashFamily64` concept
 * for more details and associated wrappers in `hash_utils.hpp`.
 */
template <typename KeyT, hash::HashFamily64 HashT> class Bloom {
  using Base = uint64_t;
  using Bitmap = std::vector<Base>;

public:
  /**
   * @brief Instantiates a new bloom filter
   *
   * @param m number of bits in the filter
   * @param h an instance of the hash family to use
   */
  Bloom(std::size_t m, HashT h)
      : bitmapSize{calcBitmapElems(m)}, bitmapBits{bitmapSize * baseSize()},
        h{std::move(h)}, bitmap(bitmapSize, 0) {}

  /**
   * @brief Inserts key into the filter
   *
   * @param key key to insert
   */
  void insert(const KeyT &key) {
    h.reset();
    for (std::size_t i = 0; i < k(); ++i) {
      const auto hResult = hash::hashType64(h, i, key);
      const auto idx = hResult % bitmapBits;
      set(idx);
    }
  }

  /**
   * @brief Checks if the filter contains the given key
   *
   * False positives are possible, but false negatives are not.
   *
   * @param key key to check
   * @return true if the filter potentially contains the key, false if filter
   * definitely does not contain the key
   */
  bool contains(const KeyT &key) const {
    h.reset();
    for (std::size_t i = 0; i < k(); ++i) {
      const auto hResult = hash::hashType64(h, i, key);
      const auto idx = hResult % bitmapBits;
      if (!at(idx)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @brief Returns number of bits in the filter
   *
   * @return number of bits comprising filter's bitmap
   */
  std::size_t capacity() const noexcept { return bitmapBits; }

  /**
   * @brief Number of hash functions the filter uses
   *
   * @return number of hash functions
   */
  std::size_t k() const noexcept { return h.size(); }

  /**
   * @brief Estimated number of elements in the filter
   *
   * @return estimated number of elements in the filter
   */
  std::size_t size() const noexcept {
    const std::size_t x = count();
    return -1 * (bitmapBits / k()) * std::log(1 - x / bitmapBits);
  }

  /**
   * @brief Returns number of bits set in the filter's bitmap
   *
   * @return number of bits set in the filter's bitmap
   */
  std::size_t count() const {
    return std::accumulate(
        bitmap.begin(), bitmap.end(), 0,
        [](const auto &a, const auto &b) { return a + std::popcount(b); });
  }

  /**
   * @brief Swaps the contents of this filter with another one
   *
   * @param other filter to swap with
   */
  void swap(Bloom &other) {
    std::swap(bitmapSize, other.bitmapSize);
    std::swap(bitmapBits, other.bitmapBits);
    std::swap(h, other.h);
    bitmap.swap(other.bitmap);
  }

  /**
   * @brief Clears filter's bitmap
   */
  void clear() noexcept { std::fill(bitmap.begin(), bitmap.end(), 0); }

protected:
  // non-const to allow for swapping
  std::size_t bitmapSize;
  std::size_t bitmapBits;

  mutable HashT h;
  Bitmap bitmap;

  bool at(std::size_t index) const {
    const auto &res = std::div(static_cast<long long>(index), baseSize());
    return bitmap.at(res.quot) & (static_cast<Base>(0x01) << res.rem);
  }

  void set(std::size_t index) {
    const auto &res = std::div(static_cast<long long>(index), baseSize());
    bitmap[res.quot] |= (static_cast<Base>(0x01) << res.rem);
  }

  static constexpr std::size_t baseSize() { return sizeof(Base) * 8; }

  static constexpr std::size_t calcBitmapElems(std::size_t reqBits) {
    return (reqBits / baseSize()) + 1;
  }
};

/**
 * @brief Utility function to calculate the required size of the bloom
 * filter's bitmap size to store the maximum number of elements with a given
 * collision probability
 *
 * @param maxElements maximum number of elements the filter will store
 * @param collisionProb desired collision probability (range: 0.0 - 1.0) - the
 * smaller, the better
 *
 * @return m - number of bits in the filter
 */
std::size_t calcSize(std::size_t maxElements, double collisionProb) {
  return static_cast<std::size_t>(
      std::ceil(maxElements * std::log(collisionProb)) /
      std::log(1 / std::pow(2, std::log(2))));
}

/**
 * @brief Utility function to calculate the number of hash functions required
 * to store maximum number of elements, given the size of the bloom filter's
 * bitmap
 *
 * @param bloomSize (m) size of the bloom filter's bitmap
 * @param maxElements maximum number of elements the filter will store
 *
 * @return required number of hash functions
 */
std::size_t calcHashes(std::size_t bloomSize, std::size_t maxElements) {
  return std::round((double)(std::log(2) * bloomSize) / maxElements);
}

template <typename T, hash::Hash64 HT>
auto makeWithHashWithSeedFamily(std::size_t maxElements, double collisionProb) {
  using namespace boxes::hash;
  const auto m = calcSize(maxElements, collisionProb);
  const std::size_t k = calcHashes(m, maxElements);
  return Bloom<T, HashWithSeedFamily<HT>>{m, HashWithSeedFamily<HT>{k}};
}

template <typename T, hash::Hash64 H1T, hash::Hash64 H2T>
auto makeWithKirschMitzenmacherFamily(std::size_t maxElements,
                                      double collisionProb, H1T h1, H2T h2) {
  using namespace boxes::hash;
  const auto m = calcSize(maxElements, collisionProb);
  const std::size_t k = calcHashes(m, maxElements);

  return Bloom<T, KirschMitzenmacher<H1T, H2T>>{
      static_cast<std::size_t>(m),
      makeKirschMitzenmacher(std::move(h1), std::move(h2), k)};
}

} // namespace boxes::filter::bloom

#endif // __BOXES_BLOOM_FILTER_HPP__
