/**
 * Copyright 2024 Tomasz Wisniewski <twdev.blogger@gmail.com>
 *
 * @file hash_utils.hpp
 *
 * @brief Contains utilities for instantiating hash functions and hash families
 */
#ifndef __BOXES_HASH_UTILS_HPP__
#define __BOXES_HASH_UTILS_HPP__

#include <cstdint>
#include <random>
#include <set>
#include <string>
#include <vector>

namespace boxes::hash {

/**
 * @brief Defines a minimal interface for Hashing functor
 *
 * Each hash functor of type `T` must implement the following interface:
 * - `operator()(const uint8_t *data, std::size_t len)`: updates the hash with a
 * new chunk of data
 * - `final() -> uint64_t`: returns the final hash value
 * - `reset()`: resets the hash state
 *
 * @tparam T type of the hash functor
 */
template <typename T>
concept Hash64 = requires(T t, const uint8_t *data, std::size_t len) {
  { t(data, len) };
  { t.final() } -> std::same_as<uint64_t>;
  { t.reset() };
};

/**
 * @brief Defines a minimal interface for HashFamily functor
 *
 * HashFamily is a functor that applies a family of hash functions to a given
 * data chunk.  Each hash family functor of type `T` must implement the
 * following interface:
 *  - `operator()(std::size_t idx, const uint8_t *data, std::size_t len)`:
 * updates the ith hash with a new chunk of data
 *  - `final(std::size_t i) -> uint64_t`: returns the final hash value for the
 * ith hash  function
 *  - `reset()`: resets the hash state of all hash functions
 *  - `size() -> std::size_t`: returns the number of hash functions in the
 * family
 *
 * @tparam T type of the hash family functor
 */
template <typename T>
concept HashFamily64 =
    requires(T t, std::size_t i, const uint8_t *data, std::size_t len) {
      { t(i, data, len) };
      { t.final(i) } -> std::same_as<uint64_t>;
      { t.reset() };
      { t.size() } -> std::same_as<std::size_t>;
    };

/**
 * @brief Applies a given hash family to a type `T`
 *
 * This is a generic overload for an adapter that applies a hash family to an
 * instance of type `T`.
 *
 * @tparam HashFamilyT type of the hash family functor
 * @tparam T type of the data to be hashed
 * @param h hash family functor instance
 * @param idx index of the hash function in the family
 * @param t data to be hashed
 * @return uint64_t hash value
 */
template <typename HashFamilyT, typename T>
uint64_t hashType64(HashFamilyT &h, std::size_t idx, const T &t) {
  h(idx, reinterpret_cast<const uint8_t *>(&t), sizeof(T));
  return h.final(idx);
}

/**
 * @brief Applies a given hash family to a string
 *
 * This is an overload for an adapter that applies a hash family to a string.
 *
 * @tparam HashFamilyT type of the hash family functor
 * @param h  hash family functor instance
 * @param idx index of the hash function in the family
 * @param s string to be hashed
 * @return uint64_t hash value
 */
template <typename HashFamilyT>
uint64_t hashType64(HashFamilyT &h, std::size_t idx, const std::string &s) {
  h(idx, reinterpret_cast<const uint8_t *>(s.data()), s.size());
  return h.final(idx);
}

/**
 * @brief Applies a given hash family to a vector of type `T`
 *
 * @tparam HashFamilyT type of the hash family functor
 * @tparam T type of the data stored in the vector
 * @param h hash family functor instance
 * @param idx  index of the hash function in the family
 * @param v vector of data to be hashed
 * @return hash value
 */
template <typename HashFamilyT, typename T>
uint64_t hashType64(HashFamilyT &h, std::size_t idx, const std::vector<T> &v) {
  h(reinterpret_cast<const uint8_t *>(v.data()), v.size() * sizeof(T));
  return h.final(idx);
}

/**
 * @brief Applies a given hash family to a vector of strings
 *
 * @tparam HashFamilyT type of the hash family functor
 * @param h hash family functor instance
 * @param idx index of the hash function in the family
 * @param v vector of strings to be hashed
 * @return hash value
 */
template <typename HashFamilyT>
uint64_t hashType64(HashFamilyT &h, std::size_t idx,
                    const std::vector<std::string> &v) {
  for (const auto &s : v) {
    h(reinterpret_cast<const uint8_t *>(s.data()), s.size());
  }
  return h.final(idx);
}

/**
 * @brief Instantiates a hash function that requires a seed with a randomly
 * generated seed
 *
 * @tparam HT type of the hash functor
 * @return HT hash functor instance
 */
template <Hash64 HT> HT makeHashWithRandomSeed() {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<uint64_t> dis;
  return HT{dis(gen)};
}

/**
 * @brief Abstracts a Kirsch-Mitzenmacher hash family functor
 *
 * Kirsh-Mitzenmacher introduces an optimisation where two independent hash
 * functions can be used to generate k hash functions.  More details available
 * in paper: https://www.eecs.harvard.edu/~michaelm/postscripts/tr-02-05.pdf
 *
 * @tparam H1 type of the first hash function
 * @tparam H2 type of the second hash function
 */
template <Hash64 H1, Hash64 H2> class KirschMitzenmacher {
public:
  /**
   * @brief Abstracts a Kirsch-Mitzenmacher hash family functor
   *
   * Kirsh-Mitzenmacher introduces an optimisation where two independent hash
   * functions can be used to generate k hash functions.  More details available
   * in paper: https://www.eecs.harvard.edu/~michaelm/postscripts/tr-02-05.pdf
   *
   * @param h1 first hash function instance
   * @param h2 second hash function instance
   * @param k number of hash functions to emulate
   */
  explicit KirschMitzenmacher(H1 h1, H2 h2, std::size_t k)
      : h1{std::move(h1)}, h2{std::move(h2)}, k{k} {}

  void reset() {
    h1.reset();
    h2.reset();
  }

  void operator()(std::size_t idx, const uint8_t *data, std::size_t len) {
    if (idx == 0) {
      h1(data, len);
      h2(data, len);
    }
  }

  uint64_t final(std::size_t i) const { return h1.final() + i * h2.final(); }

  std::size_t size() const noexcept { return k; }

protected:
  H1 h1;
  H2 h2;
  std::size_t k;
};

/**
 * @brief Abstracts a hash family functor that requires a seed n times with
 * different seed.
 *
 * @tparam HashT type of the hash functor
 */
template <Hash64 HashT> class HashWithSeedFamily {
public:
  /**
   * @brief Defines a hash family functor that uses a given hash functor
   * requiring a seed and instantiates it n times with random seeds.
   *
   * @param n number of hash functions in the family
   */
  explicit HashWithSeedFamily(std::size_t n) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<uint64_t> dis;
    std::set<uint64_t> seeds;

    for (std::size_t i = 0; i < n; ++i) {
      uint64_t seed = 0;
      do {
        // make sure all seeds are unique
        seed = dis(gen);
      } while (!seeds.insert(seed).second);
      hashes.emplace_back(dis(gen));
    }
  }

  void reset() {
    for (auto &h : hashes) {
      h.reset();
    }
  }

  std::size_t size() const noexcept { return hashes.size(); }

  void operator()(std::size_t idx, const uint8_t *data, std::size_t len) {
    hashes.at(idx)(data, len);
  }

  uint64_t final(std::size_t i) const { return hashes.at(i).final(); }

protected:
  std::vector<HashT> hashes;
};

/**
 * @brief Helper function to instantiate a Kirsch-Mitzenmacher hash family
 *
 * @param h1
 * @param h2
 * @param k
 * @return
 */
template <Hash64 H1, Hash64 H2>
KirschMitzenmacher<H1, H2> makeKirschMitzenmacher(H1 h1, H2 h2, std::size_t k) {
  return KirschMitzenmacher<H1, H2>{std::move(h1), std::move(h2), k};
}

} // namespace boxes::hash

#endif // __BOXES_HASH_UTILS_HPP__
