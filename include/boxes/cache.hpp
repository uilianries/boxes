/**
 * Copyright 2024 Tomasz Wisniewski <twdev.blogger@gmail.com>
 *
 * @file cache.hpp
 *
 * @brief Implements generic cache with pluggable eviction policies
 *
 * @example simple_lru_example.cpp
 *
 */
#ifndef __BOXES_CACHE_HPP__
#define __BOXES_CACHE_HPP__

#include "compiler.hpp"

#include <list>
#include <map>
#include <memory>
#include <unordered_map>

namespace boxes {
namespace cache {

namespace policy {

template <typename K, typename V> class LRU {
public:
  virtual ~LRU() = default;

  /**
   * @brief Construct a new LRU object with the given maximum size
   *
   * Attempts to insert more than maxSize elements into the cache will result in
   * eviction of the least recently used element.
   *
   * @param maxSize maximum number of elements the cache can hold
   */
  LRU(std::size_t maxSize) : maxSize{maxSize} {}

  using AccessQueue = std::list<K>;
  using Iterator = typename AccessQueue::iterator;

  struct Value {
    V value;
    Iterator it;
  };

  /**
   * @brief Clears the eviction policy
   */
  virtual void clear() { access.clear(); }

  virtual void onEvict(const K &, Value &value) { access.erase(value.it); }

  virtual void onAccess(const K &key, Value &value) {
    access.erase(value.it);
    value.it = access.insert(access.begin(), key);
  }

  virtual Value onInsert(const K &key, V value) {
    return Value{std::move(value), access.insert(access.begin(), key)};
  }

  /**
   * @brief Returns the reference to the first element in the cache
   *
   * The first element is the Most Recently Used (MRU) element.  LRU policy
   * stores elements in descending order of recency with the most recently used
   * element at the front of the queue.
   *
   * @return reference to the first element in the cache
   */
  virtual K &front() { return access.front(); }

  /**
   * @brief Returns the reference to the last element in the cache
   *
   * The last element is the Least Recently Used (LRU) element.  LRU policy
   * stores elements in descending order of recency with the least recently used
   * element at the back of the queue.
   *
   * @return reference to the last element in the cache
   */
  virtual K &back() { return access.back(); }

  /**
   * @brief Returns an iterator to the first element in the cache
   *
   * The elements are ordered by recency with the most recently used element at
   * the front.
   *
   * @return iterator to the first element in the cache
   */
  virtual Iterator begin() { return access.begin(); }

  /**
   * @brief Returns an iterator to the element following the last element in the
   *
   * @return iterator to the element following the last element in the cache
   */
  virtual Iterator end() { return access.end(); }

  virtual const K &getKey(Iterator it) { return *it; }

  /**
   * @brief Elects the least recently used element for eviction
   *
   * If there's still space in the cache and eviction is not needed, nullptr is
   * returned to indicate that no element has been elected for eviction.
   *
   * @return pointer to the least recently used element, or nullptr if no
   * eviction is needed
   */
  virtual const K *elect() {
    if (access.size() < maxSize) {
      return nullptr;
    }
    return &this->access.back();
  }

protected:
  const std::size_t maxSize;
  AccessQueue access;
};

/**
 * @brief Eviction policy that never evicts elements
 *
 * The order of elements in the cache is defined by insertion order.
 */
template <typename K, typename V> class NoEviction {
public:
  using AccessQueue = std::list<K>;
  using Iterator = typename AccessQueue::iterator;

  struct Value {
    V value;
    Iterator it;
  };

  /**
   * @brief Never elects an element for eviction
   *
   * @return nullptr
   */
  const K *elect() { return nullptr; }

  /**
   * @brief Clears the eviction policy
   */
  void clear() { access.clear(); }

  void onEvict(const K &, Value &) {}

  void onAccess(const K &, Value &) {}

  Value onInsert(const K &key, V value) {
    return Value{std::move(value), access.insert(access.end(), key)};
  }

  /**
   * @brief Returns the reference to the first element in the cache
   *
   * The order of elements in the cache is defined by insertion order.
   * The first element is the first element inserted into the cache.
   *
   * @return reference to the first element in the cache
   */
  K &front() { return access.front(); }

  /**
   * @brief Returns the reference to the last element in the cache
   *
   * The order of elements in the cache is defined by insertion order.
   * The last element is the last element inserted into the cache.
   *
   * @return reference to the last element in the cache
   */
  K &back() { return access.back(); }

  /**
   * @brief Returns an iterator to the first element in the cache
   *
   * @return iterator to the first element in the cache
   */
  Iterator begin() { return access.begin(); }

  /**
   * @brief Returns an iterator to the element following the last element in the
   * cache
   *
   * @return iterator to the element following the last element in the cache
   */
  Iterator end() { return access.end(); }

  const K &getKey(Iterator it) { return *it; }

protected:
  AccessQueue access;
};

/**
 * @brief Constructs a new MRU eviction policy
 *
 * Attempts to insert more than maxSize elements into the cache will result in
 * eviction of the most recently used element.
 *
 * When iterating over the cache, the most recently used (or inserted) element
 * is stored at the back of the queue.
 *
 * Both LRU and MRU policies return the least likely to be evicted element as
 * the `front` element.
 */
template <typename K, typename V> class MRU : public LRU<K, V> {
public:
  using LRU<K, V>::LRU;

  void onAccess(const K &key, typename LRU<K, V>::Value &value) override {
    this->access.erase(value.it);
    value.it = this->access.insert(this->access.rbegin().base(), key);
  }

  typename LRU<K, V>::Value onInsert(const K &key, V value) override {
    return typename LRU<K, V>::Value{
        std::move(value),
        this->access.insert(this->access.rbegin().base(), key)};
  }
};

/**
 * @brief Eviction policy that evicts the least frequently used element
 *
 * The order of elements in the cache is defined by the frequency of access.
 * The most frequently used element (and least likely to be evicted) is stored
 * at the front of the queue.
 *
 */
template <typename K, typename V> class LFU {
public:
  explicit LFU(std::size_t maxSize) : maxSize{maxSize} {}

  using FrequencyMap = std::multimap<std::size_t, K>;
  using Iterator = typename FrequencyMap::reverse_iterator;

  struct Value {
    V value;
    Iterator it;
  };

  /**
   * @brief Clears the eviction policy
   */
  void clear() { freqs.clear(); }

  void onAccess(const K &key, Value &value) {
    auto it = value.it.base();
    const auto freq = it->first;
    freqs.erase(it);
    value.it = std::make_reverse_iterator(freqs.emplace(freq + 1, key));
  }

  Value onInsert(const K &key, V &&value) {
    constexpr std::size_t initialFreq = 0;
    auto rit = std::make_reverse_iterator(freqs.emplace(initialFreq, key));
    return Value{std::forward<V>(value), rit};
  }

  void onEvict(const K &, Value &value) { freqs.erase(value.it.base()); }

  const K &getKey(Iterator it) { return it->second; }

  /**
   * @brief Returns the reference to the first element in the cache
   *
   * First element is the most frequently used element.
   *
   * @return most frequently used element
   */
  K &front() { return freqs.rbegin()->second; }

  /**
   * @brief Returns the reference to the last element in the cache
   *
   * Last element is the least frequently used element.
   *
   * @return least frequently used element
   */
  K &back() { return freqs.begin()->second; }

  /**
   * @brief Returns an iterator to the first element in the cache
   *
   * First element is the most frequently used element.
   *
   * @return iterator to the first element in the cache
   */
  Iterator begin() { return freqs.rbegin(); }

  /**
   * @brief Returns an iterator to the element following the last element in the
   * cache
   *
   * @return iterator to the element following the last element in the cache
   */
  Iterator end() { return freqs.rend(); }

  /**
   * @brief Elects the least frequently used element for eviction
   *
   * @return pointer to the least frequently used element, or nullptr if no
   * eviction is needed
   */
  const K *elect() {
    if (freqs.size() < maxSize) {
      return nullptr;
    }

    return &freqs.begin()->second;
  }

protected:
  const std::size_t maxSize;
  FrequencyMap freqs;
};

} // namespace policy

/**
 * @brief Generic cache with pluggable eviction policies
 *
 * @tparam K key type (must be hashable, comparable and copyable)
 * @tparam V value type
 * @tparam CachePolicy eviction policy to use
 */
template <typename K, typename V,
          template <typename, typename> class CachePolicy>
class Cache {
public:
  using Policy = CachePolicy<K, V>;
  using PolicyIterator = typename Policy::Iterator;

  class iterator {
  public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = std::pair<K, V>;
    using difference_type = std::ptrdiff_t;

    iterator(Cache<K, V, CachePolicy> &cache, PolicyIterator it)
        : cache{cache}, it{it} {}

    iterator &operator++() {
      ++it;
      return *this;
    }

    iterator operator++(int) {
      iterator copy = *this;
      ++it;
      return copy;
    }

    iterator &operator--() {
      --it;
      return *this;
    }

    iterator operator--(int) {
      iterator copy = *this;
      --it;
      return copy;
    }

    value_type &operator*() {
      const auto &key = cache.policy.getKey(it);
      const auto &pv = cache.data[key];
      valueProxy = std::make_pair(key, pv.value);
      return valueProxy;
    }

    value_type *operator->() {
      const auto &key = cache.policy.getKey(it);
      auto &pv = cache.data[key];
      valueProxy = std::make_pair(key, pv.value);
      return &valueProxy;
    }

    bool operator==(const iterator &other) const { return it == other.it; }

    bool operator!=(const iterator &other) const { return it != other.it; }

  protected:
    Cache<K, V, CachePolicy> &cache;
    PolicyIterator it;
    std::pair<K, V> valueProxy;
  };

  /**
   * @brief Construct a new Cache object
   *
   * @param policy Eviction policy to use
   */
  explicit Cache(Policy policy) : policy{std::move(policy)} {}

  /**
   * @brief Reserve space for the cache to avoid unnecessary reallocations
   *
   * @param size number of elements to reserve space for
   */
  void reserve(std::size_t size) { data.reserve(size); }

  /**
   * @brief Returns the number of elements in the cache
   *
   * @return number of elements in the cache
   */
  std::size_t size() const { return data.size(); }

  /**
   * @brief Returns the maximum number of elements the cache can ever hold
   *
   * This is the upper water mark for the cache as constrained by platform
   * implementation.
   *
   * This number is not synonymous with the cache size, as restricted by the
   * eviction policy.
   *
   * @return maximum number of elements the cache can ever hold
   */
  std::size_t max_size() const { return data.max_size(); }

  /**
   * @brief Returns whether the cache is empty
   *
   * @return true if the cache is empty, false otherwise
   */
  bool empty() const { return data.empty(); }

  /**
   * @brief Removes all elements from the cache
   */
  void clear() {
    data.clear();
    policy.clear();
  }

  /**
   * @brief Inserts a key-value pair into the cache
   *
   * If the key is already in the cache, the value is updated and the key is
   * refreshed in accordance with the eviction policy.
   *
   * @param key key to insert
   * @param value value to insert
   * @return true if the key was inserted, false if the key was already in the
   * cache
   */
  template <typename KArg, typename VArg>
  bool insert(KArg &&key, VArg &&value) {
    if (auto it = data.find(key); it != data.end()) {
      policy.onAccess(key, it->second);
      return false;
    } else {
      // evict if needed
      if (auto elected = policy.elect()) {
        if (auto it = data.find(*elected); it != data.end()) {
          policy.onEvict(*elected, it->second);
          data.erase(it);
        }
      }

      data.emplace(key, policy.onInsert(key, std::forward<VArg>(value)));
      return true;
    }

    boxes_unreachable();
  }

  /**
   * @brief Check if the cache contains a key
   *
   * @param key key to check for
   * @return true if the key is in the cache, false otherwise
   */
  bool contains(const K &key) const { return data.find(key) != data.end(); }

  /**
   * @brief Returns an iterator to the first element in the cache
   *
   * The iteration order is defined by the eviction policy.
   *
   * @return iterator to the first element in the cache
   */
  iterator begin() { return iterator{*this, policy.begin()}; }

  /**
   * @brief Returns an iterator to the element following the last element in the
   * cache
   *
   * The iteration order is defined by the eviction policy.
   *
   * @return iterator to the element following the last element in the cache
   */
  iterator end() { return iterator{*this, policy.end()}; }

  /**
   * @brief Returns a const iterator to the first element in the cache
   *
   * @return const iterator to the first element in the cache
   */
  iterator cbegin() const { return const_cast<Cache *>(this)->begin(); }

  /**
   * @brief Returns a const iterator to the element following the last element
   * in the cache
   *
   * @return const iterator to the element following the last element in the
   * cache
   */
  iterator cend() const { return const_cast<Cache *>(this)->end(); }

  /**
   * @brief Returns an iterator to the element with the given key
   *
   * The key, if present, is refreshed in accordance with the eviction policy.
   *
   * @param key key to search for
   * @return iterator to the element with the given key, or end() if the key is
   * not in the cache
   */
  iterator find(const K &key) {
    if (auto it = data.find(key); it != data.end()) {
      policy.onAccess(key, it->second);
      return iterator{*this, it->second.it};
    }
    return end();
  }

  /**
   * @brief Returns a reference to the value associated with the given key
   *
   * The key, if present, is refreshed in accordance with the eviction policy.
   *
   * If the key is not in the cache, an exception is thrown.
   *
   * @param key key to search for
   * @return reference to the value associated with the given key
   */
  V &at(const K &key) {
    if (auto it = data.find(key); it != data.end()) {
      policy.onAccess(key, it->second);
      return it->second.value;
    }

    throw std::out_of_range{"Key not found"};
  }

  /**
   * @brief Returns a const reference to the value associated with the given key
   *
   * @param key key to search for
   * @return reference to the value associated with the given key
   */
  const V &at(const K &key) const { return const_cast<Cache *>(this)->at(key); }

  /**
   * @brief Returns the reference the first element in the cache
   *
   * The order of elements in the cache is defined by the eviction policy. Refer
   * to the specific eviction policy for details.
   *
   * @return reference to the first element in the cache
   */
  K &front() { return policy.front(); }

  /**
   * @brief Returns the reference the last element in the cache
   *
   * The order of elements in the cache is defined by the eviction policy. Refer
   * to the specific eviction policy for details.
   *
   * @return reference to the key of the last element in the cache
   */
  K &back() { return policy.back(); }

  /**
   * @brief Returns const reference the first element in the cache
   *
   * @return const reference to the first element in the cache
   */
  const K &front() const { return const_cast<Cache *>(this)->front(); }

  /**
   * @brief Returns const reference the last element in the cache
   *
   * @return const reference to the last element in the cache
   */
  const K &back() const { return const_cast<Cache *>(this)->back(); }

protected:
  using Value = typename Policy::Value;
  using Map = std::unordered_map<K, Value>;

  mutable Map data;
  mutable Policy policy;
};

/**
 * @brief Create a new LRU cache
 *
 * @tparam K key type (must be hashable, comparable and copyable)
 * @tparam V
 * @param size maximum number of elements the cache can hold
 * @return Cache<K, V, LRU>
 */
template <typename K, typename V>
Cache<K, V, policy::LRU> makeLRU(std::size_t size) {
  return Cache<K, V, policy::LRU>{policy::LRU<K, V>{size}};
}

/**
 * @brief Create a new MRU cache
 *
 * @tparam K key type (must be hashable, comparable and copyable)
 * @tparam V value type
 * @param size maximum number of elements the cache can hold
 * @return Cache<K, V, MRU>
 */
template <typename K, typename V>
Cache<K, V, policy::MRU> makeMRU(std::size_t size) {
  return Cache<K, V, policy::MRU>{policy::MRU<K, V>{size}};
}

/**
 * @brief Create a new no eviction cache
 *
 * @tparam K key type (must be hashable, comparable and copyable)
 * @tparam V value type
 * @return Cache<K, V, NoEviction>
 */
template <typename K, typename V>
Cache<K, V, policy::NoEviction> makeNoEviction() {
  return Cache<K, V, policy::NoEviction>{policy::NoEviction<K, V>{}};
}

/**
 * @brief Create a new LFU cache
 *
 * @tparam K key type (must be hashable, comparable and copyable)
 * @tparam V value type
 * @param size maximum number of elements the cache can hold
 * @return Cache<K, V, LFU>
 */
template <typename K, typename V>
Cache<K, V, policy::LFU> makeLFU(std::size_t size) {
  return Cache<K, V, policy::LFU>{policy::LFU<K, V>{size}};
}

} // namespace cache
} // namespace boxes

#endif // __BOXES_CACHE_HPP__
