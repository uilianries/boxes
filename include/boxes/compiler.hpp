#ifndef __BOXES_COMPILER_HPP__
#define __BOXES_COMPILER_HPP__

#ifdef __has_include
#if __has_include(<version>)
#include <version>
#endif
#endif

#ifdef __cpp_lib_unreachable
#include <utility>
#define boxes_unreachable() std::unreachable()

#else
#include <exception>
#define boxes_unreachable() std::terminate()

#endif

#endif // __BOXES_COMPILER_HPP__
