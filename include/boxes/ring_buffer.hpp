/**
 * Copyright 2024 Tomasz Wisniewski <twdev.blogger@gmail.com>
 *
 * @file ring_buffer.hpp
 *
 * @brief Implements a fixed-size double-ended queue
 *
 * @example ring_buffer_example.cpp
 *
 */
#ifndef __BOXES_RING_BUFFER_HPP__
#define __BOXES_RING_BUFFER_HPP__

#include <array>
#include <stdexcept>

namespace boxes {

/**
 * @brief Implements a fixed-size double ended queue
 *
 * Elements can be added to the front of the buffer in O(1) time, and removed
 * from the back in O(1) time as well.  Additionally, the buffer implements
 * iterators to allow traversal of the contents.
 *
 * @tparam T The type of the elements to be stored in the buffer
 * @tparam SizeV The maximum number of elements that can be stored in the buffer
 */
template <typename T, std::size_t SizeV> class RingBuffer {
public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;

  static constexpr size_type Size = SizeV;

  class iterator {
  public:
    using value_type = T;
    using reference = T &;
    using pointer = T *;
    using difference_type = std::ptrdiff_t;
    using iterator_category = std::random_access_iterator_tag;

    iterator(RingBuffer<T, SizeV> *cnt, size_type pos) : cnt{cnt}, pos{pos} {}

    iterator &operator++() {
      if (pos != cnt->head) {
        pos = RingBuffer<T, SizeV>::nextPos(pos);
      }
      return *this;
    }

    iterator operator++(int) {
      iterator tmp{*this};
      ++(*this);
      return tmp;
    }

    iterator &operator--() {
      if (pos != cnt->tail) {
        pos = RingBuffer<T, SizeV>::prevPos(pos);
      }
      return *this;
    }

    iterator operator--(int) {
      iterator tmp{*this};
      --(*this);
      return tmp;
    }

    reference operator*() { return cnt->buffer[pos]; }

    pointer operator->() { return &cnt->buffer[pos]; }

    bool operator==(const iterator &other) const {
      return cnt == other.cnt && pos == other.pos;
    }

    bool operator!=(const iterator &other) const { return !(*this == other); }

  private:
    RingBuffer<T, SizeV> *cnt;
    size_type pos;
  };

  RingBuffer() : head{0}, tail{0} {}

  /**
   * @brief Copy constructs a RingBuffer from another
   *
   * @param other
   */
  RingBuffer(const RingBuffer<T, SizeV> &other)
      : head{other.head}, tail{other.tail}, buffer{other.buffer} {}

  /**
   * @brief Move constructs a RingBuffer from another
   *
   * @param other
   */
  RingBuffer(RingBuffer<T, SizeV> &&other)
      : head{other.head}, tail{other.tail}, buffer{std::move(other.buffer)} {
    other.head = 0;
    other.tail = 0;
  }

  /**
   * @brief Copy the contents of another RingBuffer into this one
   *
   * @param other
   * @return Reference to this RingBuffer
   */
  RingBuffer<T, SizeV> &operator=(const RingBuffer<T, SizeV> &other) {
    if (this != &other) {
      RingBuffer<T, SizeV> tmp{other};
      swap(tmp);
    }
    return *this;
  }

  /**
   * @brief Moves the contents of another RingBuffer into this one
   *
   * @param other
   * @return Reference to this RingBuffer
   */
  RingBuffer<T, SizeV> &operator=(RingBuffer<T, SizeV> &&other) {
    if (this != &other) {
      RingBuffer<T, SizeV> tmp{std::move(other)};
      swap(tmp);
    }
    return *this;
  }

  /**
   * @brief Returns an iterator to the first element in the RingBuffer
   *
   * The first element in the RingBuffer is the one that was inserted first, is
   * retrievable with the `front` method.
   *
   * @return an iterator to the first element in the RingBuffer
   */
  iterator begin() { return iterator(this, tail); }

  /**
   * @brief Returns an iterator to the end of the RingBuffer
   *
   * @return an iterator to the end of the RingBuffer
   */
  iterator end() { return iterator(this, head); }

  /**
   * @brief Returns a const iterator to the first element in the RingBuffer
   *
   * @return an iterator to the first element in the RingBuffer
   */
  iterator cbegin() const { return begin(); }

  /**
   * @brief Returns a const iterator to the end of the RingBuffer
   *
   * @return an iterator to the end of the RingBuffer
   */
  iterator cend() const { return end(); }

  /**
   * @brief Returns true if the RingBuffer is empty
   *
   * @return true if the RingBuffer is empty
   */
  bool empty() const noexcept { return head == tail; }

  /**
   * @brief Returns true if the RingBuffer is full
   *
   * @return true if the RingBuffer is full
   */
  bool full() const noexcept { return nextHead() == tail; }

  /**
   * @brief Returns the last element in the queue
   *
   * Since this RingBuffer is a FIFO, the `back` element is the one that was
   * most recently inserted into the RingBuffer.
   *
   * @return A reference to the last element in the queue
   */
  T &back() {
    if (empty()) {
      throw std::out_of_range("RingBuffer::front");
    }
    return buffer[prevHead()];
  }

  /**
   * @brief Returns the first element in the queue
   *
   * Since this RingBuffer is a FIFO, the `front` element is the one that was
   * inserted into the RingBuffer first.
   *
   * @return A reference to the first element in the queue
   */
  T &front() {
    if (empty()) {
      throw std::out_of_range("RingBuffer::back");
    }
    return buffer[tail];
  }

  /**
   * @brief Version of `front` that can be used with const RingBuffers
   *
   * @return A reference to the first element in the queue
   */
  const T &front() const { return const_cast<RingBuffer *>(this)->front(); }

  /**
   * @brief Version of `back` that can be used with const RingBuffers
   *
   * @return A reference to the last element in the queue
   */
  const T &back() const { return const_cast<RingBuffer *>(this)->back(); }

  /**
   * @brief Allows for random access to the elements in the RingBuffer
   *
   * The element at position `pos` is the `pos`th element inserted with `pos`th
   * push_back call.
   *
   * No bounds checking is performed.  It's safe to use this method with `pos`
   * greater than or equal to the size of the RingBuffer. If `pos` is greater
   * than or equal to the size of the RingBuffer, the `pos` will wrap around.
   *
   * @param pos The position of the element to be accessed
   * @return A reference to the element at position `pos`
   */
  T &operator[](size_type pos) { return buffer[(tail + pos) % BuffSize]; }

  /**
   * @brief Returns a const reference to the element at position `pos`
   *
   * @param pos The position of the element to be accessed
   * @return A const reference to the element at position `pos`
   */
  const T &operator[](size_type pos) const {
    return const_cast<RingBuffer *>(this)->operator[](pos);
  }

  /**
   * @brief Allows for random access to the elements in the RingBuffer
   *
   * The semantics of this method are the same as the `operator[]` method, but
   * bounds checking is performed.  `std::out_of_range` will be thrown if `pos`
   * is greater than or equal to the size of the RingBuffer.
   *
   * @param pos The position of the element to be accessed
   * @return A reference to the element at position `pos`
   */
  T &at(size_type pos) {
    if (pos >= size()) {
      throw std::out_of_range("RingBuffer::at");
    }
    return (*this)[pos];
  }

  /**
   * @brief Const version of `at`
   *
   * @param pos The position of the element to be accessed
   * @return A const reference to the element at position `pos`
   */
  const T &at(size_type pos) const {
    return const_cast<RingBuffer *>(this)->at(pos);
  }

  /**
   * @brief Returns true if the contents of this RingBuffer are equal to another
   *
   * Performs a lexicographical comparison of the contents of this RingBuffer.
   *
   * @param other The RingBuffer to compare with
   * @return true if the contents of this RingBuffer are equal to another
   */
  bool operator==(const RingBuffer<T, SizeV> &other) const {
    if (size() != other.size()) {
      return false;
    }

    for (size_type i = 0; i < size(); ++i) {
      if ((*this)[i] != other[i]) {
        return false;
      }
    }

    return true;
  }

  /**
   * @brief Returns true if the contents of this RingBuffer are not equal to
   * another
   *
   * Performs a lexicographical comparison of the contents of this RingBuffer.
   *
   * @param other The RingBuffer to compare with
   * @return true if the contents of this RingBuffer are not equal to another
   */
  bool operator!=(const RingBuffer<T, SizeV> &other) const {
    return !(*this == other);
  }

  /**
   * @brief Swaps the contents of this RingBuffer with another
   *
   * Required to implement the Container requirements of the C++ standard.
   *
   * @param other The RingBuffer to swap with
   */
  void swap(RingBuffer<T, SizeV> &other) {
    std::swap(head, other.head);
    std::swap(tail, other.tail);
    buffer.swap(other.buffer);
  }

  /**
   * @brief Inserts a new element at the end of the RingBuffer
   *
   * Newly inserted elements will be placed at the end of the RingBuffer.  Newly
   * inserted element can be accessed using the `back` method.
   *
   * @tparam Arg The type of the element to be inserted
   * @param value The value of the element to be inserted
   * @return true if the element was successfully inserted
   */
  template <typename Arg> bool push_back(Arg &&value) {
    const size_type next = nextHead();
    if (next != tail) {
      buffer[head] = std::forward<Arg>(value);
      head = next;
      return true;
    }
    return false;
  }

  /**
   * @brief Inserts a new element at the front of the RingBuffer
   *
   * Newly inserted elements will be placed at the front of the RingBuffer.
   * Newly inserted element can be accessed using the `front` method.
   *
   * @tparam Arg The type of the element to be inserted
   * @param value The value of the element to be inserted
   * @return true if the element was successfully inserted
   */
  template <typename Arg> bool push_front(Arg &&value) {
    const size_type next = prevTail();
    if (next != head) {
      tail = next;
      buffer[tail] = std::forward<Arg>(value);
      return true;
    }
    return false;
  }

  /**
   * @brief Removes the last element from the RingBuffer
   *
   * The element to be removed is the element that was most recently inserted
   * with `push_back`.
   *
   * @return true if an element was removed.  If the RingBuffer is empty, this
   * will return false.
   */
  bool pop_back() {
    if (empty()) {
      return false;
    }
    head = prevHead();
    buffer[head] = std::move(T{});
    return true;
  }

  /**
   * @brief Removes the first element from the RingBuffer
   *
   * The element to be removed is the element retrieable with the `front`
   * method.
   *
   * @return true if an element was removed.  If the RingBuffer is empty, this
   * will return false.
   */
  bool pop_front() {
    if (empty()) {
      return false;
    }
    buffer[tail] = std::move(T{});
    tail = nextTail();
    return true;
  }

  /**
   * @brief Returns the number of elements in the RingBuffer
   *
   * @return The number of elements in the RingBuffer
   */
  size_type size() const noexcept {
    if (head >= tail) {
      return head - tail;
    }
    return BuffSize - tail + head;
  }

  /**
   * @brief Clears the contents of the RingBuffer
   *
   * This method will remove all elements from the RingBuffer, leaving it in an
   * empty state.
   */
  void clear() {
    while (pop_front()) {
    }
    head = 0;
    tail = 0;
  }

protected:
  static constexpr size_type BuffSize = Size + 1;
  size_type head;
  size_type tail;
  std::array<T, BuffSize> buffer;

  constexpr static size_type nextPos(size_type pos) noexcept {
    return (pos + 1) % BuffSize;
  }

  constexpr static size_type prevPos(size_type pos) noexcept {
    return (pos + BuffSize - 1) % BuffSize;
  }

  constexpr size_type prevHead() const noexcept { return prevPos(head); }
  constexpr size_type nextHead() const noexcept { return nextPos(head); }
  constexpr size_type nextTail() const noexcept { return nextPos(tail); }
  constexpr size_type prevTail() const noexcept { return prevPos(tail); }
};

} // namespace boxes

#endif // __BOXES_RING_BUFFER_HPP__
