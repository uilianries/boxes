/**
 * Copyright 2024 Tomasz Wisniewski <twdev.blogger@gmail.com>
 *
 * @file hashes.hpp
 *
 * @brief Contains wrappers for various hash functions that can be used in
 * Bloom filters.
 */
#ifndef __BOXES_HASHES_HPP__
#define __BOXES_HASHES_HPP__

#include <xxhash.h>

#include <memory>

namespace boxes::hash {

/**
 * @class XXHash64
 *
 * @brief boxes wrapper for the xxhash64 algorithm
 *
 * This wrapper adapts the xxhash implementation to Hash64 concept
 *
 * https://xxhash.com/
 */
class XXHash64 {
public:
  explicit XXHash64(uint64_t seed = 0);

  void reset();

  void operator()(const uint8_t *key, std::size_t len);

  uint64_t final() const;

protected:
  uint64_t seed;
  std::unique_ptr<XXH64_state_t, decltype(&XXH64_freeState)> state;
};

/**
 * @class Murmur2A_X64_64
 *
 * @brief a boxes wrapper for the Murmur2A algorithm
 *
 * This wrapper adapts the Murmur2A implementation to Hash64 concept
 *
 * https://en.wikipedia.org/wiki/MurmurHash
 */
class Murmur2A_X64_64 {
public:
  explicit Murmur2A_X64_64(uint64_t seed);

  void reset();

  void operator()(const uint8_t *key, std::size_t len);

  uint64_t final() const;

protected:
  uint64_t seed;
  uint64_t h;
};

/**
 * @class BJOneAtATime
 *
 * @brief a boxes wrapper for Bob Jenkins' One-at-a-Time algorithm
 *
 * This wrapper adapts the One-at-a-Time implementation to Hash64 concept
 *
 * http://www.burtleburtle.net/bob/hash/doobs.html
 * https://en.wikipedia.org/wiki/Jenkins_hash_function
 *
 */
class BJOneAtATime {
public:
  explicit BJOneAtATime(uint64_t seed);

  void reset();

  void operator()(const uint8_t *key, std::size_t length);

  uint64_t final() const;

protected:
  uint32_t hash;
  uint32_t seed;
};

} // namespace boxes::hash

#endif // __BOXES_HASHES_HPP__
