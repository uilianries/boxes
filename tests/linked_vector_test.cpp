#include <gtest/gtest.h>

#include <boxes/linked_vector.hpp>

using namespace boxes;

class LinkedVectorTest : public ::testing::Test {
public:
  void SetUp() override {}
  void TearDown() override {}

protected:
};

class X {
public:
  X() { destroyed = false; }
  ~X() { destroyed = true; }
  static bool destroyed;
};

bool X::destroyed = false;

TEST_F(LinkedVectorTest, test_DestructionOnPop) {
  LinkedVector<std::unique_ptr<X>, 4096> linked_vector{};
  linked_vector.push_back(std::make_unique<X>());
  linked_vector.pop_back();
  ASSERT_TRUE(X::destroyed);
}

TEST_F(LinkedVectorTest, test_PushPopOnTrivialType) {
  LinkedVector<int, 4096> linked_vector{};
  auto value = 1234;
  linked_vector.push_back(value);
  ASSERT_EQ(linked_vector.front(), value);
  ASSERT_EQ(linked_vector.back(), value);
  ASSERT_EQ(linked_vector.size(), 1);

  linked_vector.pop_back();
  ASSERT_EQ(linked_vector.size(), 0);
}

TEST_F(LinkedVectorTest, test_PushPopFront) {
  LinkedVector<int, 4096> linked_vector{};
  const int v[] = {1, 2, 3, 4};
  for (auto i : v) {
    linked_vector.push_back(i);
  }

  ASSERT_EQ(linked_vector.front(), v[0]);
  ASSERT_EQ(linked_vector.back(), v[3]);

  linked_vector.pop_back();
  ASSERT_EQ(linked_vector.front(), v[0]);
  ASSERT_EQ(linked_vector.back(), v[2]);

  linked_vector.pop_front();
  ASSERT_EQ(linked_vector.front(), v[1]);
  ASSERT_EQ(linked_vector.back(), v[2]);
}

TEST_F(LinkedVectorTest, test_ifCapacityGrowsWithPushOperations) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  ASSERT_EQ(linked_vector.capacity(), chunkSize);
  ASSERT_EQ(linked_vector.size(), 0);

  linked_vector.push_front(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_front(4);

  ASSERT_EQ(linked_vector.capacity(), chunkSize);
  ASSERT_EQ(linked_vector.size(), 4);
  ASSERT_EQ(linked_vector.front(), 4);
  ASSERT_EQ(linked_vector.back(), 3);

  linked_vector.push_back(5);
  ASSERT_EQ(linked_vector.capacity(), chunkSize * 2);
  ASSERT_EQ(linked_vector.size(), 5);

  linked_vector.push_front(6);
  ASSERT_EQ(linked_vector.capacity(), chunkSize * 3);
  ASSERT_EQ(linked_vector.size(), 6);

  ASSERT_EQ(linked_vector.front(), 6);
  ASSERT_EQ(linked_vector.back(), 5);
}

TEST_F(LinkedVectorTest, test_ifDirectAccessReturnsCorrectValues) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  ASSERT_EQ(linked_vector[0], 1);
  ASSERT_EQ(linked_vector[1], 2);
  ASSERT_EQ(linked_vector[2], 3);
  ASSERT_EQ(linked_vector[3], 4);

  linked_vector.push_back(5);
  ASSERT_EQ(linked_vector.capacity(), chunkSize * 2);
  ASSERT_EQ(linked_vector[0], 1);
  ASSERT_EQ(linked_vector[1], 2);
  ASSERT_EQ(linked_vector[2], 3);
  ASSERT_EQ(linked_vector[3], 4);
  ASSERT_EQ(linked_vector[4], 5);

  linked_vector.push_front(6);
  ASSERT_EQ(linked_vector.capacity(), chunkSize * 3);
  ASSERT_EQ(linked_vector[0], 6);
  ASSERT_EQ(linked_vector[1], 1);
  ASSERT_EQ(linked_vector[2], 2);
  ASSERT_EQ(linked_vector[3], 3);
  ASSERT_EQ(linked_vector[4], 4);
  ASSERT_EQ(linked_vector[5], 5);
}

TEST_F(LinkedVectorTest, test_ifAtThrowsWhenOutOfRange) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  ASSERT_EQ(linked_vector.at(0), 1);
  ASSERT_EQ(linked_vector.at(1), 2);
  ASSERT_EQ(linked_vector.at(2), 3);
  ASSERT_EQ(linked_vector.at(3), 4);

  ASSERT_THROW(linked_vector.at(4), std::out_of_range);
}

TEST_F(LinkedVectorTest, test_ifClearRemovesAllElements) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  linked_vector.clear();
  ASSERT_EQ(linked_vector.size(), 0);
  ASSERT_EQ(linked_vector.capacity(), chunkSize);
}

TEST_F(LinkedVectorTest, test_ifEmptyReturnsCorrectValue) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  ASSERT_TRUE(linked_vector.empty());
  linked_vector.push_back(1);
  ASSERT_FALSE(linked_vector.empty());
  linked_vector.pop_back();
  ASSERT_TRUE(linked_vector.empty());
}

TEST_F(LinkedVectorTest, test_ifTrimFrontRemovesEmptyChunks) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  linked_vector.pop_front();
  linked_vector.pop_front();
  linked_vector.pop_front();
  linked_vector.pop_front();

  ASSERT_EQ(linked_vector.size(), 0);
  ASSERT_EQ(linked_vector.capacity(), chunkSize);

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  linked_vector.pop_back();
  linked_vector.pop_back();
  linked_vector.pop_back();
  linked_vector.pop_back();

  ASSERT_EQ(linked_vector.size(), 0);
  ASSERT_EQ(linked_vector.capacity(), chunkSize);
}

TEST_F(LinkedVectorTest, test_ifTrimBackRemovesEmptyChunks) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  linked_vector.pop_back();
  linked_vector.pop_back();
  linked_vector.pop_back();
  linked_vector.pop_back();

  ASSERT_EQ(linked_vector.size(), 0);
  ASSERT_EQ(linked_vector.capacity(), chunkSize);

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  linked_vector.pop_front();
  linked_vector.pop_front();
  linked_vector.pop_front();
  linked_vector.pop_front();

  ASSERT_EQ(linked_vector.size(), 0);
  ASSERT_EQ(linked_vector.capacity(), chunkSize);
}

TEST_F(LinkedVectorTest, test_ifItsPossibleToIterateOverTheContents) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector{};

  linked_vector.push_back(1);
  linked_vector.push_back(2);
  linked_vector.push_back(3);
  linked_vector.push_back(4);

  std::vector<int> v;
  for (auto i : linked_vector) {
    v.push_back(i);
  }

  ASSERT_EQ(v.size(), 4);
  ASSERT_EQ(v[0], 1);
  ASSERT_EQ(v[1], 2);
  ASSERT_EQ(v[2], 3);
  ASSERT_EQ(v[3], 4);
}

TEST_F(LinkedVectorTest, test_ifComparisonOperatorsWork) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector1{};
  LinkedVector<int, chunkSize> linked_vector2{};

  linked_vector1.push_back(1);
  linked_vector1.push_back(2);
  linked_vector1.push_back(3);
  linked_vector1.push_back(4);

  linked_vector2.push_back(1);
  linked_vector2.push_back(2);
  linked_vector2.push_back(3);
  linked_vector2.push_back(4);

  ASSERT_TRUE(linked_vector1 == linked_vector2);
  ASSERT_FALSE(linked_vector1 != linked_vector2);

  linked_vector2.push_back(5);
  ASSERT_TRUE(linked_vector1 != linked_vector2);
  ASSERT_FALSE(linked_vector1 == linked_vector2);
}

TEST_F(LinkedVectorTest,
       test_ifSubsequentPushFrontsDontCreateAdditionalChunks) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> linked_vector1{};

  linked_vector1.push_front(1);
  linked_vector1.push_front(2);
  linked_vector1.push_front(3);
  linked_vector1.push_front(4);
  ASSERT_EQ(linked_vector1.capacity(), chunkSize);

  linked_vector1.push_front(5);
  ASSERT_EQ(linked_vector1.capacity(), chunkSize * 2);

  linked_vector1.push_front(6);
  linked_vector1.push_front(7);
  linked_vector1.push_front(8);
  ASSERT_EQ(linked_vector1.capacity(), chunkSize * 2);

  linked_vector1.push_front(9);
  ASSERT_EQ(linked_vector1.capacity(), chunkSize * 3);
}

TEST_F(LinkedVectorTest, test_ifFrontBackOrderIsCorrect) {
  constexpr std::size_t chunkSize = 4;
  LinkedVector<int, chunkSize> lv{};

  lv.push_front(1);
  ASSERT_EQ(lv.front(), 1);
  ASSERT_EQ(lv.back(), 1);

  lv.push_front(2);
  ASSERT_EQ(lv.front(), 2);
  ASSERT_EQ(lv.back(), 1);
  ASSERT_EQ(lv[0], 2);
  ASSERT_EQ(lv[1], 1);

  lv.push_back(3);
  ASSERT_EQ(lv.front(), 2);
  ASSERT_EQ(lv.back(), 3);
  ASSERT_EQ(lv[0], 2);
  ASSERT_EQ(lv[1], 1);
  ASSERT_EQ(lv[2], 3);

  lv.push_back(4);
  ASSERT_EQ(lv.front(), 2);
  ASSERT_EQ(lv.back(), 4);
  ASSERT_EQ(lv[0], 2);
  ASSERT_EQ(lv[1], 1);
  ASSERT_EQ(lv[2], 3);
  ASSERT_EQ(lv[3], 4);

  lv.push_front(5);
  ASSERT_EQ(lv.front(), 5);
  ASSERT_EQ(lv.back(), 4);
  ASSERT_EQ(lv[0], 5);
  ASSERT_EQ(lv[1], 2);
  ASSERT_EQ(lv[2], 1);
  ASSERT_EQ(lv[3], 3);
  ASSERT_EQ(lv[4], 4);
}
