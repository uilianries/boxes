#include <gtest/gtest.h>

#include <boxes/cache.hpp>

using namespace boxes;

class CacheTest : public ::testing::Test {
public:
protected:
};

TEST_F(CacheTest, test_ConstructionWithAllPolicies) {
  cache::makeLRU<int, int>(3);
  cache::makeMRU<int, int>(3);
  cache::makeLFU<int, int>(3);
  cache::makeNoEviction<int, int>();
}

TEST_F(CacheTest, test_LFUOrderingAfterAccessOperation) {
  auto cache = cache::makeLFU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  ASSERT_EQ(cache.front(), 1);
  ASSERT_EQ(cache.back(), 2);
}

TEST_F(CacheTest, test_LFUOrderingAfterInsertOperation) {
  auto cache = cache::makeLFU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  cache.insert(4, 4);

  ASSERT_EQ(cache.front(), 1);
  ASSERT_EQ(cache.back(), 4);
}

TEST_F(CacheTest, test_LFUOrderingAfterInsertOperationWithEviction) {
  auto cache = cache::makeLFU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  cache.insert(4, 4);
  cache.insert(5, 5);

  ASSERT_EQ(cache.front(), 1);
  ASSERT_EQ(cache.back(), 5);
}

TEST_F(CacheTest, test_ifLRUOrderingAfterAccessOperationIsCorrect) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  ASSERT_EQ(cache.front(), 3);
  ASSERT_EQ(cache.back(), 1);
}

TEST_F(CacheTest, test_ifLRUOrderingAfterInsertOperationIsCorrect) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  cache.insert(4, 4);

  ASSERT_EQ(cache.front(), 4);
  ASSERT_EQ(cache.back(), 2);
}

TEST_F(CacheTest, test_ifLRUOrderingAfterInsertOperationWithEvictionIsCorrect) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  cache.insert(4, 4);
  cache.insert(5, 5);

  ASSERT_EQ(cache.front(), 5);
  ASSERT_EQ(cache.back(), 3);
}

TEST_F(CacheTest, test_ifMRUOrderingAfterAccessOperationIsCorrect) {
  auto cache = cache::makeMRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  ASSERT_EQ(cache.front(), 1);
  ASSERT_EQ(cache.back(), 3);
}

TEST_F(CacheTest, test_ifMRUOrderingAfterInsertOperationIsCorrect) {
  auto cache = cache::makeMRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  cache.insert(4, 4);

  ASSERT_EQ(cache.front(), 1);
  ASSERT_EQ(cache.back(), 4);
}

TEST_F(CacheTest, test_ifMRUOrderingAfterInsertOperationWithEvictionIsCorrect) {
  auto cache = cache::makeMRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.insert(3, 3);

  cache.at(1);
  cache.at(2);
  cache.at(1);
  cache.at(1);
  cache.at(3);
  cache.at(1);
  cache.at(2);
  cache.at(3);
  cache.at(3);

  cache.insert(4, 4);
  cache.insert(5, 5);

  ASSERT_EQ(cache.front(), 1);
  ASSERT_EQ(cache.back(), 5);
}

TEST_F(CacheTest, test_ifCacheIsEmpty) {
  auto cache = cache::makeLRU<int, int>(3);

  ASSERT_TRUE(cache.empty());
  ASSERT_EQ(cache.size(), 0);
}

TEST_F(CacheTest, test_ifCacheIsNotEmpty) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 1);

  ASSERT_FALSE(cache.empty());
  ASSERT_EQ(cache.size(), 1);
}

TEST_F(CacheTest, test_ifCacheSizeIsCorrect) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);

  ASSERT_EQ(cache.size(), 2);
}

TEST_F(CacheTest, test_ifCacheSizeIsZeroAfterClear) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 1);
  cache.insert(2, 2);
  cache.clear();

  ASSERT_EQ(cache.size(), 0);
  ASSERT_TRUE(cache.empty());
}

TEST_F(CacheTest, test_ifContainsReturnsTrue) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  ASSERT_TRUE(cache.contains(1));
  ASSERT_TRUE(cache.contains(2));
  ASSERT_TRUE(cache.contains(3));
  ASSERT_FALSE(cache.contains(4));
}

TEST_F(CacheTest, test_ifInsertionOfMoveOnlyTypeIsPossible) {
  auto cache = cache::makeLRU<int, std::unique_ptr<int>>(3);
  cache.insert(1, std::make_unique<int>(10));
  cache.insert(2, std::make_unique<int>(20));
  cache.insert(3, std::make_unique<int>(30));

  ASSERT_TRUE(cache.contains(1));
  ASSERT_TRUE(cache.contains(2));
  ASSERT_TRUE(cache.contains(3));
  ASSERT_FALSE(cache.contains(4));
}

TEST_F(CacheTest, test_ifFindReturnsCorrectValue) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  auto it = cache.find(1);
  ASSERT_NE(it, cache.end());
  ASSERT_EQ(it->first, 1);
  ASSERT_EQ(it->second, 10);

  auto it2 = cache.find(20);
  ASSERT_EQ(it2, cache.end());
}

TEST_F(CacheTest, test_ifLRUIterationOrderIsCorrect) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  auto it = cache.begin();
  ASSERT_EQ(it->first, 3);
  ASSERT_EQ(it->second, 30);
  ++it;
  ASSERT_EQ(it->first, 2);
  ASSERT_EQ(it->second, 20);
  ++it;
  ASSERT_EQ(it->first, 1);
  ASSERT_EQ(it->second, 10);
  ++it;
  ASSERT_EQ(it, cache.end());
}

TEST_F(CacheTest, test_ifLFUIterationOrderIsCorrect) {
  auto cache = cache::makeLFU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  ASSERT_NO_THROW(cache.at(3));
  ASSERT_NO_THROW(cache.at(3));
  ASSERT_NO_THROW(cache.at(1));

  auto it = cache.begin();
  ASSERT_EQ(it->first, 3);
  ASSERT_EQ(it->second, 30);
  ++it;
  ASSERT_EQ(it->first, 1);
  ASSERT_EQ(it->second, 10);
  ++it;
  ASSERT_EQ(it->first, 2);
  ASSERT_EQ(it->second, 20);
  ++it;
  ASSERT_EQ(it, cache.end());
}

TEST_F(CacheTest, test_ifMRUIterationOrderIsCorrect) {
  auto cache = cache::makeMRU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  auto it = cache.begin();
  ASSERT_EQ(it->first, 1);
  ASSERT_EQ(it->second, 10);

  ++it;
  ASSERT_EQ(it->first, 2);
  ASSERT_EQ(it->second, 20);

  ++it;
  ASSERT_EQ(it->first, 3);
  ASSERT_EQ(it->second, 30);

  ++it;
  ASSERT_EQ(it, cache.end());
}

TEST_F(CacheTest,
       test_ifNoEvictionCacheInsertsCorrectlyAndIteratesInInsertionOrder) {
  auto cache = cache::makeNoEviction<int, int>();
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);
  cache.insert(4, 30);
  cache.insert(5, 30);

  ASSERT_EQ(cache.size(), 5);

  auto it = cache.begin();
  ASSERT_EQ(it->first, 1);
  ASSERT_EQ(it->second, 10);

  ++it;
  ASSERT_EQ(it->first, 2);
  ASSERT_EQ(it->second, 20);

  ++it;
  ASSERT_EQ(it->first, 3);
  ASSERT_EQ(it->second, 30);

  ++it;
  ASSERT_EQ(it->first, 4);
  ASSERT_EQ(it->second, 30);

  ++it;
  ASSERT_EQ(it->first, 5);
  ASSERT_EQ(it->second, 30);
}

TEST_F(CacheTest, test_ifNoEvictionCacheInsertsCorrectlyWithMoveOnlyType) {
  auto cache = cache::makeNoEviction<int, std::unique_ptr<int>>();
  cache.insert(1, std::make_unique<int>(10));
  cache.insert(2, std::make_unique<int>(20));
  cache.insert(3, std::make_unique<int>(30));
  cache.insert(4, std::make_unique<int>(30));
  cache.insert(5, std::make_unique<int>(30));

  ASSERT_EQ(cache.size(), 5);
}

TEST_F(CacheTest, test_ifLRUIterationWithForLoopWorks) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  int i = 0;
  for (const auto &[key, value] : cache) {
    if (i == 0) {
      ASSERT_EQ(key, 3);
      ASSERT_EQ(value, 30);
    } else if (i == 1) {
      ASSERT_EQ(key, 2);
      ASSERT_EQ(value, 20);
    } else if (i == 2) {
      ASSERT_EQ(key, 1);
      ASSERT_EQ(value, 10);
    }
    i++;
  }
}

TEST_F(CacheTest, test_ifFindRefreshesLRUOrder) {
  auto cache = cache::makeLRU<int, int>(3);
  cache.insert(1, 10);
  cache.insert(2, 20);
  cache.insert(3, 30);

  cache.find(1);
  cache.insert(4, 40);

  auto it = cache.begin();
  ASSERT_EQ(it->first, 4);
  ASSERT_EQ(it->second, 40);

  ++it;
  ASSERT_EQ(it->first, 1);
  ASSERT_EQ(it->second, 10);

  ++it;
  ASSERT_EQ(it->first, 3);
  ASSERT_EQ(it->second, 30);

  ++it;
  ASSERT_EQ(it, cache.end());
}
